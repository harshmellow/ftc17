package org.firstinspires.ftc.teamcode;

/**
 * Created by Ben on 1/17/2017.
 *
 * Container class used to hold controller inputs and times.
 */

public class ControllerInputData {
    public enum PadButton {
        A,
        B,
        X,
        Y,
        LEFT_BUMPER,
        RIGHT_BUMPER,
        START,
        DPAD_LEFT,
        DPAD_RIGHT,
        DPAD_UP,
        DPAD_DOWN
    }

    public enum PadValue {
        LEFT_TRIGGER,
        RIGHT_TRIGGER,
        LEFT_ANALOG_X,
        LEFT_ANALOG_Y,
        RIGHT_ANALOG_X,
        RIGHT_ANALOG_Y,
    }

    public boolean isButton = false;
    public PadButton padButton = null;
    public PadValue padValue = null;
    public float value = 0;
    public float time = 0;

    public ControllerInputData() {
    }

    public ControllerInputData(PadButton b, float time) {
        this.isButton = true;
        this.padButton = b;
        this.time = time;
    }

    public ControllerInputData(PadValue v, float value, float time) {
        this.isButton = false;
        this.padValue = v;
        this.value = value;
        this.time = time;
    }
}
