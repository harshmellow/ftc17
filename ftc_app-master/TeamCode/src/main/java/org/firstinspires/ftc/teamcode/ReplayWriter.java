package org.firstinspires.ftc.teamcode;

import android.content.Context;

import com.qualcomm.robotcore.hardware.Gamepad;

import java.io.FileOutputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

/**
 * Created by Ben on 1/17/2017.
 *
 * Add entries to an object of this class, then use WriteFile() to write the file.
 */

public class ReplayWriter {
    public ReplayFile replayFile;
    private float startTime = 0;

    public ReplayWriter() {
        this.replayFile = new ReplayFile();
    }

    //Sets time replay should start from
    public void SetStartTime(float time) {
        this.startTime = time;
    }

    //Monitors the specified gamepad during the frame called; time should be seconds since startTime
    public void MonitorController(Gamepad gamepad, float time) {
        float actualTime = time - startTime;
        //Record button presses
        if (gamepad.a)
            this.replayFile.AddInput(ControllerInputData.PadButton.A, actualTime);
        if (gamepad.b)
            this.replayFile.AddInput(ControllerInputData.PadButton.B, actualTime);
        if (gamepad.x)
            this.replayFile.AddInput(ControllerInputData.PadButton.X, actualTime);
        if (gamepad.y)
            this.replayFile.AddInput(ControllerInputData.PadButton.Y, actualTime);
        if (gamepad.dpad_left)
            this.replayFile.AddInput(ControllerInputData.PadButton.DPAD_LEFT, actualTime);
        if (gamepad.dpad_right)
            this.replayFile.AddInput(ControllerInputData.PadButton.DPAD_RIGHT, actualTime);
        if (gamepad.dpad_up)
            this.replayFile.AddInput(ControllerInputData.PadButton.DPAD_UP, actualTime);
        if (gamepad.dpad_down)
            this.replayFile.AddInput(ControllerInputData.PadButton.DPAD_DOWN, actualTime);
        if (gamepad.left_bumper)
            this.replayFile.AddInput(ControllerInputData.PadButton.LEFT_BUMPER, actualTime);
        if (gamepad.right_bumper)
            this.replayFile.AddInput(ControllerInputData.PadButton.RIGHT_BUMPER, actualTime);

        //Record controller values
        double cutoff = 0.01;
        if (Math.abs(gamepad.left_trigger) > cutoff)
            this.replayFile.AddInput(ControllerInputData.PadValue.LEFT_TRIGGER, gamepad.left_trigger, actualTime);
        if (Math.abs(gamepad.right_trigger) > cutoff)
            this.replayFile.AddInput(ControllerInputData.PadValue.RIGHT_TRIGGER, gamepad.right_trigger, actualTime);
        if (Math.abs(gamepad.left_stick_x) > cutoff)
            this.replayFile.AddInput(ControllerInputData.PadValue.LEFT_ANALOG_X, gamepad.left_stick_x, actualTime);
        if (Math.abs(gamepad.left_stick_y) > cutoff)
            this.replayFile.AddInput(ControllerInputData.PadValue.LEFT_ANALOG_Y, gamepad.left_stick_y, actualTime);
        if (Math.abs(gamepad.right_stick_x) > cutoff)
            this.replayFile.AddInput(ControllerInputData.PadValue.RIGHT_ANALOG_X, gamepad.right_stick_x, actualTime);
        if (Math.abs(gamepad.right_stick_y) > cutoff)
            this.replayFile.AddInput(ControllerInputData.PadValue.RIGHT_ANALOG_Y, gamepad.right_stick_y, actualTime);
    }
}
