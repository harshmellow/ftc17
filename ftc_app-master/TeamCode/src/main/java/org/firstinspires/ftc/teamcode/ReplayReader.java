package org.firstinspires.ftc.teamcode;

import android.content.Context;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.util.ArrayList;

/**
 * @author Ben
 * Commented by Marcelo so other's could read
 * Created by Ben on 1/17/2017.
 *
 * Read input data from this class.
 */

public class ReplayReader {
    public boolean finished = false;
    public float startTime = 0;
    //Buttons
    public boolean a = false;
    public boolean b = false;
    public boolean x = false;
    public boolean y = false;
    //Dpad
    public boolean dpad_left = false;
    public boolean dpad_right = false;
    public boolean dpad_up = false;
    public boolean dpad_down = false;
    public boolean left_bumper = false;
    public boolean right_bumper = false;
    public float left_trigger =  0;
    public float right_trigger =  0;
    public float left_stick_x =  0;
    public float left_stick_y =  0;
    public float right_stick_x =  0;
    public float right_stick_y =  0;

    private ArrayList<ControllerInputData> inputs;

    public ReplayReader() {
        this.inputs = new ArrayList<>();
    }

    public ReplayReader(ReplayFile replayFile) {
        this.inputs = replayFile.GetArrayList();
    }

    //Updates the reader's simulated controller
    public void Update(float time) {
        float actualTime = time - this.startTime;
        this.a = false;
        this.b = false;
        this.x = false;
        this.y = false;
        this.dpad_left = false;
        this.dpad_right = false;
        this.dpad_up = false;
        this.dpad_down = false;
        this.left_bumper = false;
        this.right_bumper = false;

        this.left_trigger =  0;
        this.right_trigger =  0;
        this.left_stick_x =  0;
        this.left_stick_y =  0;
        this.right_stick_x =  0;
        this.right_stick_y =  0;

        boolean exists = false;

        double currTime = 0;
        for (int i = 0; i < this.inputs.size(); i++) {
            currTime = this.inputs.get(i).time;
            if(currTime > actualTime)
                break;
            if (Approximately(this.inputs.get(i).time, actualTime, 0.01)) {
                exists = true;
                if (this.inputs.get(i).isButton) {
                    if (this.inputs.get(i).padButton == ControllerInputData.PadButton.A)
                        this.a = true;
                    if (this.inputs.get(i).padButton == ControllerInputData.PadButton.B)
                        this.b = true;
                    if (this.inputs.get(i).padButton == ControllerInputData.PadButton.X)
                        this.x = true;
                    if (this.inputs.get(i).padButton == ControllerInputData.PadButton.Y)
                        this.y = true;
                    if (this.inputs.get(i).padButton == ControllerInputData.PadButton.DPAD_LEFT)
                        this.dpad_left = true;
                    if (this.inputs.get(i).padButton == ControllerInputData.PadButton.DPAD_RIGHT)
                        this.dpad_right = true;
                    if (this.inputs.get(i).padButton == ControllerInputData.PadButton.DPAD_UP)
                        this.dpad_up = true;
                    if (this.inputs.get(i).padButton == ControllerInputData.PadButton.DPAD_DOWN)
                        this.dpad_down = true;
                    if (this.inputs.get(i).padButton == ControllerInputData.PadButton.LEFT_BUMPER)
                        this.left_bumper = true;
                    if (this.inputs.get(i).padButton == ControllerInputData.PadButton.RIGHT_BUMPER)
                        this.right_bumper = true;
                } else {
                    if (this.inputs.get(i).padValue == ControllerInputData.PadValue.LEFT_TRIGGER)
                        this.left_trigger = this.inputs.get(i).value;
                    if (this.inputs.get(i).padValue == ControllerInputData.PadValue.RIGHT_TRIGGER)
                        this.right_trigger = this.inputs.get(i).value;
                    if (this.inputs.get(i).padValue == ControllerInputData.PadValue.LEFT_ANALOG_X)
                        this.left_stick_x = this.inputs.get(i).value;
                    if (this.inputs.get(i).padValue == ControllerInputData.PadValue.LEFT_ANALOG_Y)
                        this.left_stick_y = this.inputs.get(i).value;
                    if (this.inputs.get(i).padValue == ControllerInputData.PadValue.RIGHT_ANALOG_X)
                        this.right_stick_x = this.inputs.get(i).value;
                    if (this.inputs.get(i).padValue == ControllerInputData.PadValue.RIGHT_ANALOG_Y)
                        this.right_stick_y = this.inputs.get(i).value;
                }
            }
        }
        if(!exists)
            this.finished = true;
    }

    //Utility function to account for rounding errors
    private boolean Approximately(double val1, double val2, double fudge) {
        return Math.abs(val1 - val2) < fudge;
    }

    public int GetNumEntries() {
        return this.inputs.size();
    }

    public void SetStartTime(float startTime) {
        this.startTime = startTime;
    }
}
