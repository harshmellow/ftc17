package org.firstinspires.ftc.teamcode;

class Position {
    public float x, y;

    public Position() {
    }

    public Position(float x, float y) {
        this.x = x;
        this.y = y;
    }
}

public class NavSystem {
    private Position fieldPos;
    private float fieldRot;

    public NavSystem() {
        this.fieldPos = new Position();
    }

    public void InitVuforia() {

    }

    //Returns position relative to field
    //The position is clamped between -1 and 1
    public Position GetFieldPosition() {
        return this.fieldPos;
    }

    //Returns rotation relative to field
    //The rotation is clamped between 0 and 360
    public float GetFieldRotation() {
        return this.fieldRot;
    }

    //Moves to specified position
    //Call this every loop until the robot reaches the position
    public void MoveToPosition(Position p) {

    }

    //Rotates to specified rotation
    //Call this every loop until the robot reaches the rotation
    public void RotateToAngle(float angle) {

    }

    //Checks if robot is at specified position
    public boolean AtPosition(Position p) {
        return false;
    }

    //Checks if robot is at specified rotation
    public boolean AtRotation(float angle) {
        return false;
    }

    //Updates actions associated with the camera
    //Includes moving the camera and updating the relative transform
    public void UpdateCamera() {

    }


}
