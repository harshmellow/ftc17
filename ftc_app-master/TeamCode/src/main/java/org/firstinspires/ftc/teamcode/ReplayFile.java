package org.firstinspires.ftc.teamcode;

import android.content.Context;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

/**
 * Created by Ben on 10/24/2017.
 */

public class ReplayFile {
    private ArrayList<ControllerInputData> inputs;

    public ReplayFile() {
        this.inputs = new ArrayList<>();
    }

    //Reads replay file, returns null if read fails
    public static ReplayFile ReadFile(String fileName, Context context) {
        ReplayFile r = new ReplayFile();
        FileInputStream fstream;
        try {
            fstream = context.openFileInput(fileName);
            ObjectInputStream f = new ObjectInputStream(fstream);
            //Number of entries
            int numEntries = f.readInt();
            //Read all entries
            for(int i = 0; i < numEntries; i++) {
                ControllerInputData currData = new ControllerInputData();
                currData.isButton = f.readBoolean();
                if(currData.isButton)
                    currData.padButton = ControllerInputData.PadButton.values()[f.readInt()];
                else {
                    currData.padValue = ControllerInputData.PadValue.values()[f.readInt()];
                    currData.value = f.readFloat();
                }
                currData.time = f.readFloat();
                if(currData.isButton)
                    r.AddInput(currData.padButton, currData.time);
                else
                    r.AddInput(currData.padValue, currData.value, currData.time);
            }
            f.close();
        }
        catch (Exception e) {
            return null;
        }
        return r;
    }

    //Writes replay data to file, returns false if file write fails
    public boolean WriteFile(String fileName, Context context) {
        FileOutputStream fstream;
        try {
            fstream = context.openFileOutput(fileName, Context.MODE_PRIVATE);
            ObjectOutputStream f = new ObjectOutputStream(fstream);
            //Number of entries
            f.writeInt(this.inputs.size());
            //Write all entries
            for(int i = 0; i < this.inputs.size(); i++) {
                ControllerInputData currData = this.inputs.get(i);
                f.writeBoolean(currData.isButton);
                if(currData.isButton)
                    f.writeInt(currData.padButton.ordinal());
                else {
                    f.writeInt(currData.padValue.ordinal());
                    f.writeFloat(currData.value);
                }
                f.writeFloat(currData.time);
            }
            f.flush();
            f.close();
        }
        catch (Exception e) {
            return false;
        }
        return true;
    }

    //Adds button input data (dpad, shoulder buttons)
    public void AddInput(ControllerInputData.PadButton b, float time) {
        ControllerInputData c = new ControllerInputData(b, time);
        this.inputs.add(c);
    }

    //Adds value type input data (triggers, analog sticks)
    public void AddInput(ControllerInputData.PadValue v, float value, float time) {
        ControllerInputData c = new ControllerInputData(v, value, time);
        this.inputs.add(c);
    }

    //Returns the internal ArrayList of inputs
    public ArrayList<ControllerInputData> GetArrayList() {
        return this.inputs;
    }
}
